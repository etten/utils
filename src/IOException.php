<?php

/**
 * Copyright © 2016 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten;

/**
 * The exception that is thrown when an I/O error occurs.
 */
class IOException extends \RuntimeException
{

}
